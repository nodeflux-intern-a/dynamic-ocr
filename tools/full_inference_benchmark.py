from email import header
from mmocr.utils.ocr import MMOCR
import pandas as pd
import os
import json
det_models = [
    'DB_r18',
    'DB_r50',
    'DBPP_r50',
    'DRRG',
    'FCE_IC15',
    'FCE_CTW_DCNv2',
    'MaskRCNN_CTW',
    'MaskRCNN_IC15',
    'MaskRCNN_IC17',
    'PANet_CTW',
    'PANet_IC15',
    'PS_CTW',
    'PS_IC15',
    'TextSnake',
]
recog_models = [
    'ABINet',
    'CRNN',
    'CRNN_TPS',
    'MASTER',
    'NRTR_1/16-1/8',
    'NRTR_1/8-1/4',
    'RobustScanner',
    'SAR',
    'SATRN',
    'SATRN_sm',
    'SEG'
]
for det_model in det_models:
    for recog_model in recog_models:
        folder_path = '/home/intern-ferlanda/mmocr_v160/demo/benchmark/'
        file_name = r'ktp-demo-{}-{}-kie.jpg'.format(det_model, recog_model)
        file_path = os.path.join(folder_path, file_name)

        ocr = MMOCR(det=det_model, recog=recog_model, kie="SDMGR", kie_ckpt="models/ktp/model.pth", kie_config="models/ktp/sdmgr_unet16_200e_ktp_50.py")
        results = ocr.readtext("/home/intern-ferlanda/mmocr_v160/demo/ktp.png", details=True, merge=True, merge_xdist=25, output=file_path)
        for result in results:
            for res in result["result"]:
            # change numpy.int64 to int so json dump not error
                res["box"] = [int(i) for i in res["box"]]

        res_list = [] # final result
        for i,result in enumerate(results):
            res_list.append({})
            res_list[i]['filename'] = file_name
            res_list[i]['detector'] = det_model
            res_list[i]['recognizer'] = recog_model
            for inference_result in result["result"]:
                if not inference_result["label"] == "Others":
                    if (not inference_result["label"] in res_list[i]) :
                        res_list[i][inference_result["label"]] = []
                    res_list[i][inference_result["label"]].append(inference_result['text'])

full_result_json = json.dumps(res_list, default=str)
with open ('/home/intern-ferlanda/mmocr_v160/demo/benchmark/full_result.json', 'w') as f:
    f.write(full_result_json)
