from email import header
from mmocr.utils.ocr import MMOCR
import pandas as pd
import os
import json
import datetime
det_models = [
    'DBPP_r50'
]
recog_models = [
    'ABINet',
    'ABINet',
    'CRNN',
    'CRNN_TPS',
    'MASTER',
    'NRTR_1/16-1/8',
    'NRTR_1/8-1/4',
    'RobustScanner',
    'SAR',
    'SATRN',
    'SATRN_sm',
    'SEG'
]
full_res_list = []
for det_model in det_models:
    for recog_model in recog_models:
        cur_time = datetime.datetime.now()
        ocr = MMOCR(det=det_model, recog=recog_model)
        str_res = '{}: {}'.format(recog_model, datetime.datetime.now()-cur_time)
        full_res_list.append(str_res)
print(full_res_list)