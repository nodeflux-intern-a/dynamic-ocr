from email import header
from mmocr.utils.ocr import MMOCR
import pandas as pd
import os
import json
det_models = [
    'DB_r18',
    'DB_r50',
    'DBPP_r50',
    'DRRG',
    'FCE_IC15',
    'FCE_CTW_DCNv2',
    'MaskRCNN_CTW',
    'MaskRCNN_IC15',
    'MaskRCNN_IC17',
    'PANet_CTW',
    'PANet_IC15',
    'PS_CTW',
    'PS_IC15',
    'TextSnake',
]
recog_models = [
    'ABINet',
    'CRNN',
    'CRNN_TPS',
    'MASTER',
    'NRTR_1/16-1/8',
    'NRTR_1/8-1/4',
    'RobustScanner',
    'SAR',
    'SATRN',
    'SATRN_sm',
    'SEG'
]
full_res_list = []
for det_model in det_models:
    for recog_model in recog_models:
        folder_path = '/home/intern-ferlanda/mmocr_v160/demo/benchmark-kie-det/'
        file_name = r'ktp-demo-{}-{}.jpg'.format(det_model, recog_model)
        file_path = os.path.join(folder_path, file_name)

        ocr = MMOCR(det=det_model, recog=recog_model)
        results = ocr.readtext("/home/intern-ferlanda/mmocr_v160/demo/ktp.png", details=True, merge=True, merge_xdist=25, output=file_path)
        for result in results:
            for res in result["result"]:
            # change numpy.int64 to int so json dump not error
                res["box"] = [int(i) for i in res["box"]]

        full_res_list.append(result)
full_result_json = json.dumps(full_res_list, default=str)
with open ('/home/intern-ferlanda/mmocr_v160/demo/benchmark/full_result.json', 'w') as f:
    f.write(full_result_json)
