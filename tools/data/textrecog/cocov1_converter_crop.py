import json
import cv2
from PIL import Image
import os
root_dir = "data/ktp_coco_recog/"

def crop_images(filename,id,bbox,stage):
    img = Image.open(filename)
    img2 = img.crop(bbox)
    path = os.path.join(root_dir,"crops",stage,str(id)+".jpg")
    img2.save(path)

json_path = os.path.join(root_dir,"annotations","instances_default.json")
f = open(json_path)
json_coco = json.load(f)
images_dict = {}
for image in json_coco['images']:
    images_dict[image['id']] = image['file_name'] 

str_train = ""
str_test = ""
sz = len(json_coco['annotations'])
train_sz = int(sz*0.8)
i = 0
for line in json_coco['annotations']:
    filename = os.path.join(root_dir,"images",images_dict[line['image_id']])
    id = line['id']
    bbox = line['bbox']
    text =  '"' + line['attributes']['Isi Teks'] +'"'
    bbox = (bbox[0], bbox[1], bbox[2] + bbox[0] , bbox[1] + bbox[3])
    str_cur = '{}, {}'.format(str(id) + '.jpg', text )
    if i < train_sz:
        crop_images(filename,id,bbox,"train")
        str_train += str_cur + '\n'
    else:
        crop_images(filename,id,bbox,"test")
        str_test += str_cur + '\n'
    i += 1
train_annotation_path = os.path.join(root_dir,"annotations","Challenge2_Train_Task3_GT.txt")
test_annotation_path = os.path.join(root_dir,"annotations","Challenge2_Test_Task3_GT.txt")
with open(train_annotation_path, 'w') as f:
    f.write(str_train)
with open(test_annotation_path, 'w') as f:
    f.write(str_test)