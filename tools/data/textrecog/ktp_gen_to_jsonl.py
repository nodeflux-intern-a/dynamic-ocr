import json 

with open("data/ktp_synthetic/rec_label_punct.txt", "r") as f:
    lines = f.readlines()
train_str = ""
test_str = ""
sz = len(lines)
train_sz = int(sz * 0.8)
for i in range(train_sz):
    filename, text = lines[i].split("\t")
    line_dict = {'filename': filename, 'text': text.strip()}
    train_str += json.dumps(line_dict) + "\n"

for i in range(train_sz, sz):
    filename, text = lines[i].split("\t")
    line_dict = {'filename': filename, 'text': text.strip()}
    test_str += json.dumps(line_dict) + "\n"

with open("data/ktp_synthetic/train.jsonl", "w") as f:
    f.write(train_str)
with open("data/ktp_synthetic/test.jsonl", "w") as f:
    f.write(test_str)