from math import floor
import os 

train_files = os.listdir('data/ktp_icdar/default/annotations/test/')
print(train_files)

for files in train_files:
    print(files)
    with open('data/ktp_icdar/default/annotations/test/' + files, 'r') as f:
        str_akhir = ""
        lines = f.readlines()
        for line in lines:
            if "," in line:
                line_number = line.split(",")
                line_number = [floor(float(i)) for i in line_number]
                line_number_str = ",".join(str(i) for i in line_number)
            else:
                line_number = line.split(" ")
                line_number = [floor(float(i)) for i in line_number]
                line_number_str = " ".join(str(i) for i in line_number)
            str_akhir += line_number_str + "\n"
        with open('data/ktp_icdar/default/annotations/test/' + files, 'w') as f:
            f.write(str_akhir)
            