from math import floor
import os 

train_files = os.listdir('data/ktp_icdar/default/annotations/training/')
print(train_files)
def bounding_box(points):
    x_coordinates, y_coordinates = zip(*points)
    return [(min(x_coordinates), min(y_coordinates)), (max(x_coordinates), max(y_coordinates))]
str_hasil_akhir = ""
for files in train_files:
    print(files)
    with open('data/ktp_icdar/default/annotations/training/' + files, 'r') as f:
        str_akhir = ""
        lines = f.readlines()
        for line in lines:
            if "," in line:
                line_number = line.split(",")
                line_number = [floor(float(i)) for i in line_number]
                final_line_number = []
                for i in range(0,len(line_number),2):
                    new_point  = [line_number[i], line_number[i+1]]
                    final_line_number.append(new_point)
                converted_polygon = bounding_box(final_line_number)
                new_bbox_list = []
                for point in converted_polygon:
                    new_bbox_list.append(point[0])
                    new_bbox_list.append(point[1])
                print(new_bbox_list)
                new_str = " ".join(str(i) for i in new_bbox_list)
                str_akhir += new_str + "\n"
            else:
                str_akhir += line
        with open('data/ktp_icdar/default/annotations/training/' + files, 'w') as f:
            f.write(str_akhir)
                
