import argparse
from curses.ascii import NUL
import json
import os
import traceback
import sys
import shapely.geometry


def convert(in_file,out_dir,img_dir):
    os.makedirs(out_dir,exist_ok=True)
    #load coco json
    with open(in_file) as f:
        obj = json.load(f)
        f.close()
    #create category dictionary for dict_list.txt (used in training)

    category_dict = obj['categories']
    category_dict_filename = 'class_list.txt'
    category_dict_path = os.path.join(out_dir,category_dict_filename)

    #add every label and id
    with open(category_dict_path,"w") as f_dict:
        for category in category_dict:
            f_dict.write("{} {}".format(category['id'], category['name']))
            if(category['id'] != len(category_dict)):
                f_dict.write("\n")
        f_dict.close()

    #create image dict to help us build the mmocr json format
    image_dict = obj['images']

    #empty train dict
    train = {}

    #for every image, we will make its id as dictionary key
    for img in image_dict:
        train[img['id']] = {'file_name' : os.path.join(img_dir,img['file_name']) , 'height' : img['height'] , 'width' : img['width'] , 'annotations' : []}
    annotations = obj['annotations']
    i = 0 ;
    #append every segmentation,label and text correspond to its image id
    for annotation in annotations:
        if (len(annotation['segmentation']) == 0):
            bbox = tuple(annotation['bbox'])
            polygon=shapely.geometry.box(*(bbox[0],bbox[1],bbox[0]+bbox[2], bbox[1]+bbox[3]), ccw=True)
            K=str(polygon.wkt).split("POLYGON ((")[-1].split("))")[0].split(',')
            polygon=[]
            for m in K:
                for p in m.split(" "):
                    if p:
                        polygon.append(float(p))
            
        else:
            polygon = annotation['segmentation'][0]
            polygon = [float(p) for p in polygon]

        print(polygon,file=sys.stdout)
        train[annotation['image_id']]['annotations'].append({'box': polygon , 'text' : annotation["attributes"]["Isi Teks"] , 'label' : annotation['category_id']})

    #export the json into txt file for train
    train_filename = "train.txt"
    train_path = os.path.join(out_dir,train_filename)
    with open(train_path,"w") as f:
        fi = 1
        for train_line in train:
            f.write(str(train[train_line]).replace('\'','\"'))
            if fi != len(train):
                f.write("\n")
            fi+=1
        f.close()
    #add example for test file
    test_filename = "test.txt"
    test_path = os.path.join(out_dir,test_filename)
    with open(test_path,"w") as f:
        f.write(str(train[1]).replace('\'','\"'))
        f.close()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i','--in_file', help='Annotation file for coco json.')
    parser.add_argument('-p','--img_dir', help='Input Image Directory')
    parser.add_argument('-o','--out_dir', help='Output directory for closeset')
    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    try:
        convert(args.in_file, args.out_dir, args.img_dir)
    except Exception:
        traceback.print_exc()
    print('finish')


if __name__ == '__main__':
    main()
