
import json
import os


import streamlit as st

from mmocr.utils import ocr

os.makedirs("static", exist_ok=True)

TEMP_IMG_PATH = "static"
st.title(body="NODEFLUX INTERN TIM A")
picture = st.camera_input(label="Silahkan foto KTP anda.")
if picture:
    st.image(picture)
    if st.button(label="Submit!"):
        new_temp_img_path = os.path.join(TEMP_IMG_PATH, os.urandom(12).hex() + ".jpg")
        with open(new_temp_img_path, "wb") as f:
            f.write(picture.getvalue())
        # OCR HERE
        with st.spinner("Doing OCR ..."):
            ocr_tool = ocr.MMOCR(
                det_ckpt="models/ktp-det/model.pth",
                det_config="models/ktp-det/config.py",
                recog_ckpt="models/ktp-recog-new-1.1/model.pth",
                recog_config="models/ktp-recog-new-1.1/config.py",
                kie="SDMGR",
                kie_ckpt="models/ktp-kie/model.pth",
                kie_config="models/ktp-kie/config.py",
            )
            try:
                ocr_res = ocr_tool.readtext(new_temp_img_path, details=True,merge=True)
                for result in ocr_res:
                    for res in result["result"]:
                    # change numpy.int64 to int so json dump not error
                        res["box"] = [int(i) for i in res["box"]]
              
                # Converting the JSON Result to key valur pair JSON
                # We consider the highest confidence score as the correct value
               
                final_res_score = {}
                final_res = {}# final result
                for result in ocr_res:
                    final_res['filename'] = result['filename']
                    for inference_result in result["result"]:
                        if not inference_result["label"] == "Others":
                            if (not inference_result["label"] in final_res) :
                                final_res[inference_result["label"]] = []
                                final_res_score[inference_result["label"]] = 0
                            if (inference_result['label_score'] > final_res_score[inference_result["label"]]):
                                final_res[inference_result["label"]] = inference_result['text']
                                final_res_score[inference_result["label"]] = inference_result['label_score']
            except:
                final_res = "Error"
        st.success(body="OCR done! Click button bellow to download.")
        text_contents = json.dumps(final_res)
        st.download_button(
            label="Download OCR result", data=text_contents, file_name="OCR result.json"
        )
        os.remove(new_temp_img_path)