import json
import os
import shutil
import sys
import uuid
from mmocr.utils.ocr import MMOCR
from flask import (
    Flask,
    after_this_request,
    flash,
    make_response,
    redirect,
    render_template,
    request,
)
from werkzeug.utils import secure_filename
import streamlit as st


pwd = os.getcwd()
tempPath = "static"

app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = os.path.join(pwd, tempPath)
app.secret_key = "super secret key"


def allowed_file(filename: str) -> bool:
    return "." in filename and filename.rsplit(".", 1)[1].lower() in {
        "png",
        "jpg",
        "jpeg",
    }
    
@app.route("/upload", methods=["GET", "POST"])
def upload_files():
    if request.method == "POST":
        # Save tmp image
        imgs = request.files.getlist("uploadImg")
        project_id = uuid.uuid4().hex
        os.mkdir(os.path.join(app.config["UPLOAD_FOLDER"], project_id))
        for img in imgs:
            if img.filename == "" or img.filename is None:
                flash("No selected file")
                return redirect(request.url)
            if not allowed_file(img.filename):
                continue
            img.save(
                os.path.join(
                    app.config["UPLOAD_FOLDER"],
                    project_id,
                    secure_filename(img.filename),
                )
            )
        res = inference_document(os.path.join(app.config["UPLOAD_FOLDER"], project_id))
        print(res,file=sys.stdout)
        # Delete tmp image after response
        @after_this_request
        def remove_file(response):
            shutil.rmtree(os.path.join(app.config["UPLOAD_FOLDER"], project_id))
            return response

        # OCR
        ocr_result_str = json.dumps(res, default=str)
        response = make_response(ocr_result_str, 200, {"mimetype": "application/json"})
        response.headers["Content-Disposition"] = "attachment;filename=ktp_ocr.json"
        return response
    return render_template("index.html")

def inference_document(img_path):
    ocr = MMOCR(det="DB_r50", recog="CRNN", kie="SDMGR", kie_ckpt="models/ktp/model.pth", kie_config="models/ktp/sdmgr_unet16_200e_ktp_50.py")
    results = ocr.readtext(img_path, details=True, merge=True)
    for result in results:
        for res in result["result"]:
            # change numpy.int64 to int so json dump not error
            res["box"] = [int(i) for i in res["box"]]

    final_res = [] # final result
    for i,result in enumerate(results):
        final_res.append({})
        final_res[i]['filename'] = result['filename']
        for inference_result in result["result"]:
            if not inference_result["label"] == "Others":
                if (not inference_result["label"] in final_res[i]) :
                    final_res[i][inference_result["label"]] = []
                final_res[i][inference_result["label"]].append(inference_result['text'])
    return final_res

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5002,debug=True)
