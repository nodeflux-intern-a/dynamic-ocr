# Optical Character Recognition of KTP
This library extracts information of KTP images using MMOCR models.

## 1. Installation

First, you need to clone this repository

```
git clone git@gitlab.com:nodeflux-intern-a/dynamic-ocr.git
cd dynamic-ocr
```

### 1.1 Instalattion with Docker
Build the KTP OCR image by using provided dockerfile

```
docker build -t ktp-ocr:latest .
```

### 1.2 Manual Installation with Conda
```
conda create -n open-mmlab python=3.7 -y
conda activate open-mmlab

# install latest pytorch prebuilt with the default prebuilt CUDA version (usually the latest)
conda install pytorch==1.6.0 torchvision==0.7.0 cudatoolkit=10.1 -c pytorch

# install the latest mmcv-full
pip install mmcv-full -f https://download.openmmlab.com/mmcv/dist/cu101/torch1.6.0/index.html

# install mmdetection
pip install mmdet

pip install -r requirements.txt
pip install -v -e .  # or "python setup.py develop"
export PYTHONPATH=$(pwd):$PYTHONPATH

# for albumentations
pip install -r requirements/albu.txt

# Models & Config Download
curl -o /mmocr/models/ktp-det/  https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-det/model.pth  
curl -o /mmocr/models/ktp-det/  https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-det/config.py  
curl -o /mmocr/models/ktp-recog/ https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-recog/model.pth  
curl -o /mmocr/models/ktp-recog/ https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-recog/config.py  
curl -o /mmocr/models/ktp-kie/ https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-kie/model.pth  
curl -o /mmocr/models/ktp-kie/  https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-kie/config.py  
curl -o /mmocr/data/ktp_50/  https://dataset-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp_50/dict.txt  
curl -o /mmocr/data/ktp_50/  https://dataset-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp_50/class_list.txt 
```

## 2. How to Use

### 2.1 Demo Inference Page
![alt](/resources/take_photo.jpg)
![alt](/resources/Doing-ocr.jpg)
![alt](/resources/result_json-edited.jpg)
### 2.1.1 Using Docker
Simply, you just need to run this command
```
docker run -p 8501:8501 ktp-ocr:latest
```
Note that you can change the port.

### 2.1.2 Using Local Streamlit
Make sure you have all of the requirements installed
```
streamlit run main_st.py
```
## 2.2 Additionals

To access inside the docker container, just run this command
```
docker run --rm -it --entrypoint /bin/bash ktp-ocr:latest
```
For the training, testing, coverter, models & datasets zoo, please
refer to https://mmocr.readthedocs.io/en/latest/

# 3. Training Result

## 3.1 Detector Model

We used dataset that consist of 50 annotated KTP images (40 train, 10 test).
For the models, we used DBPP_r50, and fine tuned it with our dataset.

Plot:

![resources](/resources/text_det_loss_plot.jpg)

## 3.2 Recognizer Model

We used dataset that consist of: 
- 50 annotated KTP images (40 train, 10 test)
- 6000 Syntethic KTP Images (80% Train: 20% Test)
- SROIE Dataset

For the models, we used Masters, and fine tuned it with our dataset.

Plot:
![resources](/resources/text_rec_loss_plot.jpg)
![resources](/resources/text_recog_mean_plot.png)
## 3.3 KIE Model

We used dataset that consist of 50 annotated KTP images (40 train, 10 test).
For the models, we used SDMGR, and fine tuned it with our dataset.

Plot:
![resources](/resources/ktp_kie_macro_f1.jpg)


Citations:
```
@article{mmocr2021,
    title={MMOCR:  A Comprehensive Toolbox for Text Detection, Recognition and Understanding},
    author={Kuang, Zhanghui and Sun, Hongbin and Li, Zhizhong and Yue, Xiaoyu and Lin, Tsui Hin and Chen, Jianyong and Wei, Huaqiang and Zhu, Yiqin and Gao, Tong and Zhang, Wenwei and Chen, Kai and Zhang, Wayne and Lin, Dahua},
    journal= {arXiv preprint arXiv:2108.06543},
    year={2021}
}
```


