ARG PYTORCH="1.6.0"
ARG CUDA="10.1"
ARG CUDNN="7"

FROM pytorch/pytorch:${PYTORCH}-cuda${CUDA}-cudnn${CUDNN}-devel
ENV TORCH_CUDA_ARCH_LIST="6.0 6.1 7.0+PTX"
ENV TORCH_NVCC_FLAGS="-Xfatbin -compress-all"
ENV CMAKE_PREFIX_PATH="$(dirname $(which conda))/../"
RUN rm /etc/apt/sources.list.d/cuda.list
RUN rm /etc/apt/sources.list.d/nvidia-ml.list
RUN apt-get update && apt-get install -y git ninja-build libglib2.0-0 libsm6 libxrender-dev libxext6 libgl1-mesa-glx \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN conda clean --all
RUN pip install mmcv-full==1.5.0 -f https://download.openmmlab.com/mmcv/dist/cu101/torch1.6.0/index.html

RUN pip install mmdet==2.21.0

COPY . /mmocr
WORKDIR /mmocr

ADD  https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-det/model.pth  /mmocr/models/ktp-det/
ADD  https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-det/config.py  /mmocr/models/ktp-det/
ADD  https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-recog-new/model.pth  /mmocr/models/ktp-recog/
ADD  https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-recog-new/config.py  /mmocr/models/ktp-recog/
ADD  https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-kie/model.pth  /mmocr/models/ktp-kie/
ADD  https://models-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp-kie/config.py  /mmocr/models/ktp-kie/
ADD  https://dataset-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp_50/dict.txt  /mmocr/data/ktp_50/
ADD  https://dataset-dynamic-ocr.s3.ap-southeast-1.amazonaws.com/ktp_50/class_list.txt /mmocr/data/ktp_50/


ENV FORCE_CUDA="1"
RUN pip install -r requirements.txt
RUN pip install --no-cache-dir -e .
CMD [ "python3" , "-m" , "streamlit" , "run" , "main_st.py"]