# Text Recognition Training set, including:
# KTP Dataset (50) + SROIE (ICDAR 2019)

data_root = 'data/ktp_coco_recog'
data_root2 = 'data/sroie'
data_root3 = 'data/ktp_synthetic'

train_img_prefix = f'{data_root}/crops'
train_ann_file = f'{data_root}/train_label.jsonl'

train_img_prefix2 = f'{data_root2}/crops'
train_ann_file2 = f'{data_root2}/train_label.jsonl'

train_img_prefix3 = f'{data_root3}'
train_ann_file3 = f'{data_root3}/train_label.jsonl'

test_img_prefix = f'{data_root}/crops'
test_ann_file = f'{data_root}/test_label.jsonl'

test_img_prefix2 = f'{data_root2}/crops'
test_ann_file2 = f'{data_root2}/test_label.jsonl'

test_img_prefix3 = f'{data_root3}'
test_ann_file3 = f'{data_root3}/test_label.jsonl'

train1 = dict(
    type='OCRDataset',
    img_prefix=train_img_prefix,
    ann_file=train_ann_file,
    loader=dict(
        type='AnnFileLoader',
        repeat=1,
        file_format='txt',
        parser=dict(
            type='LineJsonParser',
            keys=['filename', 'text'],
    )),
    pipeline=None,
    test_mode=False
    )

train2 = {key: value for key, value in train1.items()}
train2['img_prefix'] = train_img_prefix2
train2['ann_file'] = train_ann_file2

train3 = {key: value for key, value in train1.items()}
train3['img_prefix'] = train_img_prefix3
train3['ann_file'] = train_ann_file3

test1 = dict(
    type='OCRDataset',
    img_prefix=test_img_prefix,
    ann_file=test_ann_file,
    loader=dict(
        type='AnnFileLoader',
        repeat=1,
        file_format='txt',
        parser=dict(
            type='LineJsonParser',
            keys=['filename', 'text'],
    )),
    pipeline=None,
    test_mode=False)
    
test2 = {key: value for key, value in test1.items()}
test2['img_prefix'] = test_img_prefix2
test2['ann_file'] = test_ann_file2

test3 = {key: value for key, value in test1.items()}
test3['img_prefix'] = test_img_prefix3
test3['ann_file'] = test_ann_file3


train_list = [train1,train2,train3]
test_list = [test1,test2,test3]
with_unknown = True


